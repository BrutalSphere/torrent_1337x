import re
from urllib.request import urlopen, Request

# Scrap 1337x web pags for torrents


def torrent_1337x(api, query):
    query = query.replace(" ", "+")
    page = "https://1337x.to"

    # Regex expressions
    pattern_url_title = re.compile(
        r'(?<=<a href=")(/torrent/.+?)(?:">)(.+?)(?:</a>)')
    pattern_seeders = re.compile(r'(?<="coll-2 seeds">)(\d+)(?=</td>)')
    pattern_leechers = re.compile(r'(?<="coll-3 leeches">)(\d+)(?=</td>)')
    pattern_size = re.compile(r'(?:<td class="coll-4 size.+?>)(.+?)(?=<)')
    pattern_magnet = re.compile(r'(?<=")magnet.+?(?=")')

    with urlopen(Request(f"{page}/search/{query}/1/", headers={"User-Agent": "Real Python"})) as result:

        # Get Page
        html = result.read().decode("utf-8")

        # Get Titles
        url_title = re.findall(pattern_url_title, html)

        # Query has 0 results
        if len(url_title) == 0:
            return ""

        # Get Seeders
        seeders = re.findall(pattern_seeders, html)

        # Get Leechers
        leechers = re.findall(pattern_leechers, html)

        # Get Size
        size = re.findall(pattern_size, html)

    # Display information for the menu
    options = [f"[S:{seeders[i]}|L:{leechers[i]}] [{size[i]}] {urlandtitle[1]}" for i,
               urlandtitle in enumerate(url_title)]

    chosen = api.menu(":torrent_1337x", options)

    # Get magnet link for desired title
    if chosen != "":
        with urlopen(Request(f"{page}{url_title[options.index(chosen)][0]}", headers={"User-Agent": "Real Python"})) as result:
            magnet = re.search(pattern_magnet, result.read().decode("utf-8"))
        return chosen + " " + magnet.group(0)
    return ""

def main(api, params):
    return torrent_1337x(api, params)